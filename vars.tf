variable "project" {
  default = "ec2-docker"
}

variable "region" {
  default = "eu-west-2"
}

variable "instance_type" {
  default = "t2.small"
}

variable "ami" {
  default = "ami-09627c82937ccdd6d" #Ubuntu 22.04 LTS
}


variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDfrX9btUObLNq6Qw7FhFJvuY/sqeRqsur0qzRx+hkLs5wrs8pmeoAsxElNn+V112UL2Zn0K4DCQIv7z3UsDehOeHxDQ0yNaap0LhDFrGag57o2kUAKtA9kKnjo3WztFGGUovoiTdLnkVwxnT94K6Xj3ZHQo+IU1O6c/+D1E6XRPkxt6/B0M2UdAuZR+AF1F4O9BfEo6IMr4E8bAnlTSaXKfYp2o4Wa18xVS4HkkFNp/4RGrOQdMI7u2MA+yVnDVsi50A8GK6Pkftd45hiUieG907kA64RiAduMY77d36HHBZkc/jFjRNgqzck0wGROq+X3oXzoqkhNRFrI9zBH/hZ0IuM8DR0HuVNOfnxfOpmRO+lMYBkWsqw/60hgWI/CDYsXWrRM0yNUvwCqdZN9mfxaUATpdfHC/W+ZWDT0kIZkwBkNO8goTWKm0gAj/ttlLzPoMeafwQclDjnRLZ8V3hiGYI+kBxBdPfIm8nZttI9T44Me6pyzOSrI04BtyUfPfLkZSfScAGeYueK/ngsi9PKKi2g+3pGYyDutlVCXSlJK7rwRQ/M+DeLqpRg96/32rrD8kXW0UVQRSMyoCknZsrxYkO+KpCltSv9/+hqzErIwd9GTOph8kY60Gr8rocrfKqIXgTKlRJvIr0hH72txxUzOwy3UVWVOc9UIUWhzK4xGqw== macuser@MacBook-Pro-Mac.local"
}

variable "root_block_volume_type" {
  default = "gp3"
}
variable "root_block_throughput" {
  default = 125
}
variable "root_block_volume_size" {
  default = 10
}
variable "root_block_iops" {
  default = 3000
}
variable "gitlab_pat_token" {}

variable "docker_username" {}

variable "docker_password" {}
