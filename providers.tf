terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.7"
    }
  }
  backend "s3" {
    bucket = "ec2-docker"
    key    = "terraform.tfstate"
    region = "eu-west-2"
    dynamodb_table = "ec2_docker_terraform_state"
  }
}

provider "aws" {
  region     = "${var.region}"    
}

data "aws_availability_zones" "azs" {
    state = "available"
}