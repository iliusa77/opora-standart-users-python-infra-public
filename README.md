This repository was created as infrastructure preparation for dockerized demo Python application

### Gitlab CI/CD variables
First need to add manually the following: 
  - AWS_ACCESS_KEY
  - AWS_SECRET_ACCESS_KEY
  - GITLAB_PAT_TOKEN
  - DOCKER_USERNAME
  - DOCKER_PASSWORD

### Prepare S3 bucket and Dynamodb for Terraform statefile and locks
Create S3 bucket for Terraform backend and DynamoDB table (Partition key: LockID) for locking and update bucket,region and dynamodb_table in providers.tf section:
```
  backend "s3" {
    bucket = "ec2-docker"
    key    = "terraform.tfstate"
    region = "eu-west-2"
    dynamodb_table = "ec2_docker_terraform_state"
  }
```

### Generate SSH pair
```
ssh-keygen -t rsa -b 4096 -f ./ec2-docker-ssh-key
chmod 400 ec2-docker-ssh-key
```

### Update Terraform variables
Define you own values in `vars.tf`

put `ec2-docker-ssh-key.pub` in public_key

`project`

`region`  and so on ...

### Infrastructire deploy 
Infra CI pipeline `.gitlab-ci.yml` will automatically Terraform validate/plan and manually Terraform apply

During deploy will be the following AWS resources:
- VPC
- Subnets
- Route Tables
- Internet Gateway
- Security Group
- EC2 ssh key pair
- EC2 instance with Docker

After deploy you can see SSH connection string in pipeline `apply` stage output like this
```
instance_ip_addr = "ssh ubuntu@<external_ec2_instance_ip> -i ec2-docker-ssh-key"
```

### Public urls and ports after deploy
- Python application: 
  - http://<external_ec2_instance_ip> 
  - http://<external_ec2_instance_ip>:8080

- Grafana: http://<external_ec2_instance_ip>:3001 (credential are in `ec2_user_data.sh` Grafana service in `monitoring-compose.yml` creation)

Optional add the following public Grafana dashboards:

   Node exporter https://grafana.com/grafana/dashboards/1860-node-exporter-full/
   
   Flask monitoring https://grafana.com/grafana/dashboards/16111-flask-monitoring/
   
   Nginx exporter https://grafana.com/grafana/dashboards/12708-nginx/
   
   Haproxy exporter https://grafana.com/grafana/dashboards/2428-haproxy/

- Prometheus: http://<external_ec2_instance_ip>:9091
- Haproxy stats: http://<external_ec2_instance_ip>:8404/stats (credential are in `ec2_user_data.sh` Haproxy config section)

### Infrastructire destroy 
Infra CI pipeline `.gitlab-ci.yml` will manually Terraform destroy after defining TF_DESTROY=True