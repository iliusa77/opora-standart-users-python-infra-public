#!/bin/bash

# Docker and compose installation
apt update
apt install -y net-tools curl docker.io haproxy git awscli
usermod -aG docker ubuntu
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Clone private Gitlab repo
cd /home/ubuntu
git clone https://iliusa77:${pat_token}@gitlab.com/iliusa77/opora-standart-users-python-application-public.git
cd /home/ubuntu/opora-standart-users-python-application-public 

# Docker login/build/push
docker login -u="${docker_username}" -p="${docker_password}"
docker build -t iliusa77/python-users-app:latest .
docker push iliusa77/python-users-app

# Configs & Docker compose files creation
cat << EOF > nginx.conf
upstream python-users {
  server 172.20.1.1:8080;
}

server {
  listen 80  default_server;
  server_name  _;

  location ~ / {
      location ~* \.(js|jsx|css|less|swf|eot|ttf|otf|woff|woff2)$ {
          add_header Cache-Control "public";
          expires +1y;
          proxy_pass http://python-users;
      }
      location ~* static.*\.(ico|jpg|jpeg|png|gif|svg)$ {
          add_header Cache-Control "public";
          expires +1y;
          proxy_pass http://python-users;
      }

      proxy_set_header        Host \$host;
      proxy_set_header        X-Real-IP \$remote_addr;
      proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for;
      proxy_set_header        X-Forwarded-Proto \$scheme;
      proxy_pass              http://python-users;
  }
}
EOF

cat << EOF > app-compose.yml
version: "3.9"

services:
  webserver:
    image: nginx
    volumes:
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
    depends_on:
      - python-users
    ports:
    - "80:80"
    networks:
      default:
        ipv4_address: 172.20.1.10

  python-users:
    image: iliusa77/python-users-app:latest
    entrypoint: ["/bin/sh", "./entrypoint.sh"]
    ports:
      - 8080
    depends_on:
      - postgresql
    links:
      - postgresql
    networks:
      default:
        ipv4_address: 172.20.1.1
    volumes:
      - .:/service

  postgresql:
    image: postgres:13.3
    environment:
      POSTGRES_DB: "users"
      POSTGRES_USER: "demouser"
      POSTGRES_PASSWORD: "demopass"
    ports:
      - "5432:5432"
    networks:
      default:
        ipv4_address: 172.20.1.2

networks:
  default:
    external:
      name: develop     
EOF

cat << EOF > prometheus_datasource.yml
apiVersion: 1

datasources:
- name: Prometheus
  type: prometheus
  url: http://prometheus:9090 
  isDefault: true
  access: proxy
  editable: true
EOF

cat << EOF > prometheus_config.yml
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 15s
alerting:
  alertmanagers:
  - static_configs:
    - targets: []
    scheme: http
    timeout: 10s
    api_version: v1
scrape_configs:
- job_name: prometheus
  honor_timestamps: true
  scrape_interval: 15s
  scrape_timeout: 10s
  metrics_path: /metrics
  scheme: http
  static_configs:
  - targets:
    - localhost:9090
- job_name: node-exporter
  scrape_interval: 15s
  static_configs:
  - targets: 
    - node-exporter:9100
- job_name: nginx-exporter
  scrape_interval: 15s
  static_configs:
  - targets: 
    - nginx-exporter:9113
EOF

cat << EOF > monitoring-compose.yml
version: "3.9"

services:
  prometheus:
    image: prom/prometheus
    container_name: prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
    ports:
      - 9090:9090
    restart: unless-stopped
    volumes:
      - ./prometheus_config.yml:/etc/prometheus/prometheus_config.yml
      - prom_data:/prometheus
    networks:
      default:
        ipv4_address: 172.20.1.3

  grafana:
    image: grafana/grafana
    container_name: grafana
    ports:
      - 3000:3000
    restart: unless-stopped
    environment:
      - GF_SECURITY_ADMIN_USER=admin
      - GF_SECURITY_ADMIN_PASSWORD=grAfAnA4095@
    volumes:
      - ./prometheus_datasource.yml:/etc/grafana/provisioning/datasources/prometheus_datasource.yml
    networks:
      default:
        ipv4_address: 172.20.1.4

  node-exporter:
    container_name: node-exporter
    image: quay.io/prometheus/node-exporter:latest
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
      - /:/host:ro,rslave
    command: 
      - '--path.rootfs=/host'
      - '--path.procfs=/host/proc' 
      - '--path.sysfs=/host/sys'
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - 9100:9100
    restart: always
    networks:
      default:
        ipv4_address: 172.20.1.5  
        
  nginx-exporter:
    image: nginx/nginx-prometheus-exporter:1.0
    container_name: nginx-exporter
    restart: always
    command:
      - --nginx.scrape-uri=http://nginx/stub_status
    ports: 
      - 9113:9113
    networks:
      default:
        ipv4_address: 172.20.1.6  
    depends_on:
      - prometheus

  haproxy-exporter:
    image: prom/haproxy-exporter
    container_name: haproxy-exporter
    restart: always
    command:
      - --haproxy.scrape-uri="http://haproxyadmin:hAprOxY9375@@localhost:8404/stats;csv"
    ports:
      - 9102:9101
    networks:
      default:
        ipv4_address: 172.20.1.7
    depends_on:
      - prometheus

volumes:
  prom_data:

networks:
  default:
    external:
      name: develop     
EOF

# External docker network creation
docker network create develop --subnet=172.20.0.0/16

# Haproxy configuration
bash -c 'cat << EOF > /etc/haproxy/haproxy.cfg
global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
        stats timeout 30s
        user haproxy
        group haproxy
        daemon

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # See: https://ssl-config.mozilla.org/#server=haproxy&server-version=2.0.3&config=intermediate
        ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
        ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
        ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets

defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

  frontend stats
    bind *:8404
    stats enable
    stats uri /stats
    stats refresh 10s
    stats auth haproxyadmin:hAprOxY9375@

  frontend users_frontend
    bind *:8080
    use_backend users_backend

  backend users_backend
    server users 172.20.1.1:8080 check

  frontend prometheus_frontend
    mode tcp
    option tcplog
    bind *:9091
    use_backend prometheus_backend

  backend prometheus_backend
    server prometheus 172.20.1.3:9090 check

  frontend grafana_frontend
    mode tcp
    option tcplog
    bind *:3001
    use_backend grafana_backend

  backend grafana_backend
    server grafana 172.20.1.4:3000 check

  frontend node_exporter_frontend
    mode tcp
    option tcplog
    bind *:9101
    use_backend node_exporter_backend

  backend node_exporter_backend
    server node_exporter 172.20.1.5:9100 check

  frontend nginx_exporter_frontend
    mode tcp
    option tcplog
    bind *:9114
    use_backend nginx_exporter_backend

  backend nginx_exporter_backend
    server nginx_exporter 172.20.1.6:9113 check
EOF'

# Start docker-compose
chown ubuntu:ubuntu /home/ubuntu/
docker-compose -f /home/ubuntu/opora-standart-test-task/app-compose.yml up -d
docker-compose -f /home/ubuntu/opora-standart-test-task/monitoring-compose.yml up -d

# Start Haproxy
systemctl enable haproxy
systemctl restart haproxy